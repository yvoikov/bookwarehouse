﻿namespace BookWarehouse.Services.Extensibility
{
    public abstract class CommandBase
    {
        public abstract void Execute();
    }
}
