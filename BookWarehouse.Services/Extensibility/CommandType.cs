﻿namespace BookWarehouse.Services.Extensibility
{
    public enum CommandType
    {
        AddBook,
        EditBook,
        ShowAllBooks,
        RemoveBook,
        SaveBooks,
        LoadBooks,
        Quit
    }
}
