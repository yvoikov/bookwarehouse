﻿using System;
using BookWarehouse.Services.Extensibility;

namespace BookWarehouse.Services.Services
{
    public class CommandService
    {
        private readonly CommandBase[] _commands;

        public CommandService(CommandBase[] commands)
        {
            _commands = commands;
        }

        public void Run()
        {
            while (true)
            {
                Console.WriteLine("Please enter a command!");
                var inputStr = Console.ReadLine();
                if (Enum.TryParse(inputStr, true, out CommandType commandType))
                {
                    _commands[(int)commandType].Execute();
                }
                else
                {
                    Console.WriteLine("Command not found!");
                }
                Console.WriteLine();
            }
        }
    }
}
