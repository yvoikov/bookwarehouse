﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using BookWareHouse.Data;
using BookWarehouse.DataAccessLayer.Extensibility.Storage;
using BookWarehouse.Services.Extensibility;

namespace BookWarehouse.Services.Commands
{
    public class CommandShowAllBooks : CommandBase
    {
        private readonly IBookGetter _bookGetter;

        public CommandShowAllBooks(IBookGetter bookGetter)
        {
            _bookGetter = bookGetter;
        }
        public override void Execute()
        {
            var books = _bookGetter.GetBooks(b => b.SerialNumber > 0);
            var options = new JsonSerializerOptions
            {
                WriteIndented = true,
            };
            Console.WriteLine(JsonSerializer.Serialize<IList<Book>>(books, options));
        }
    }
}
