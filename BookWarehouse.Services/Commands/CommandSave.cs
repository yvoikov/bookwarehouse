﻿using System;
using BookWarehouse.DataAccessLayer.Extensibility.Storage;
using BookWarehouse.Services.Extensibility;

namespace BookWarehouse.Services.Commands
{
    public class CommandSave : CommandBase
    {
        private readonly IBookSaver _bookSaver;

        public CommandSave(IBookSaver bookSaver)
        {
            _bookSaver = bookSaver;
        }
        public override void Execute()
        {
            Console.WriteLine(_bookSaver.SaveBooks()
                ? "Books saved successfully!"
                : "An error occured during the saving!");
        }
    }
}
