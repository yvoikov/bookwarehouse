﻿using BookWarehouse.Services.Extensibility;

namespace BookWarehouse.Services.Commands
{
    public class CommandQuit : CommandBase
    {
        public override void Execute()
        {
            System.Environment.Exit(0);
        }
    }
}
