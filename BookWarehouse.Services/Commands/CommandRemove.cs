﻿using System;
using BookWarehouse.DataAccessLayer.Extensibility.Storage;
using BookWarehouse.Services.Extensibility;

namespace BookWarehouse.Services.Commands
{
    public class CommandRemove : CommandBase
    {
        private readonly IBookRemover _bookRemover;
        private readonly IBookGetter _bookGetter;

        public CommandRemove(IBookRemover bookRemover, IBookGetter bookGetter)
        {
            _bookRemover = bookRemover;
            _bookGetter = bookGetter;
        }
        public override void Execute()
        {
            Console.WriteLine("Please enter the Serial number of a book that should be removed: ");
            string input = Console.ReadLine();

            if (!UInt32.TryParse(input, out var serialNumber))
            {
                Console.WriteLine("No number provided!");
                return;
            }

            var books = _bookGetter.GetBooks(b => b.SerialNumber == serialNumber);

            if (books.Count == 0)
            {
                Console.WriteLine("No book with such Serial number was found!");
            }

            _bookRemover.RemoveBooks(books[0]);
            Console.WriteLine("Book was successfully removed!");
        }
    }
}
