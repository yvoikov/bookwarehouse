﻿using System;
using BookWareHouse.Data;
using BookWarehouse.DataAccessLayer.Extensibility.Storage;
using BookWarehouse.Services.Extensibility;

namespace BookWarehouse.Services.Commands
{
    public class CommandEdit : CommandBase
    {
        private readonly IBookEditor _bookEditor;
        private readonly IBookGetter _bookGetter;

        public CommandEdit(IBookEditor bookEditor, IBookGetter bookGetter)
        {
            _bookEditor = bookEditor;
            _bookGetter = bookGetter;
        }
        public override void Execute()
        {
            Console.WriteLine("Please enter the Serial number of a book: ");
            string input = Console.ReadLine();

            if (!UInt32.TryParse(input, out var serialNumber))
            {
                Console.WriteLine("No number provided!");
                return;
            }

            var books = _bookGetter.GetBooks(b => b.SerialNumber == serialNumber);
            
            if (books.Count == 0)
            {
                Console.WriteLine("No book with such Serial number was found!");
            }

            Book book = books[0];
            Console.WriteLine("Edit the Name:");
            Console.Write(book.Name);
            input = Console.ReadLine();

            if (String.IsNullOrEmpty(input))
            {
                Console.WriteLine("No Author provided!");
                return;
            }

            book.Author = input;
            Console.WriteLine("Edit the Author:");
            Console.Write(book.Author);
            input = Console.ReadLine();

            if (String.IsNullOrEmpty(input))
            {
                Console.WriteLine("No Author provided!");
                return;
            }

            Console.WriteLine("Edit the Number of pages:");
            Console.Write(book.NumberOfPages);
            input = Console.ReadLine();

            if (!UInt32.TryParse(input, out var numberOfPages))
            {
                Console.WriteLine("No number provided!");
                return;
            }

            book.NumberOfPages = numberOfPages;
            book.Genres.Clear();
            Console.WriteLine("Please enter new genres (one per line, such as {0}) and empty string when you finish:",
                string.Join(", ", Enum.GetNames(typeof(GenreTypes))));
            input = Console.ReadLine();

            while (!String.IsNullOrEmpty(input))
            {
                if (Enum.TryParse(input, true, out GenreTypes genreType))
                {
                    if (!book.Genres.Contains(genreType))
                    {
                        book.Genres.Add(genreType);
                    }
                    else
                    {
                        Console.WriteLine("This genre is already listed!");
                    }
                }
                else
                {
                    Console.WriteLine("Please enter valid genre!");
                }

                input = Console.ReadLine();
            }

            _bookEditor.EditBook(book);

        }
    }
}
