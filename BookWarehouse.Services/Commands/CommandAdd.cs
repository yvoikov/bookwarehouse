﻿using System;
using BookWareHouse.Data;
using BookWarehouse.DataAccessLayer.Extensibility.Storage;
using BookWarehouse.Services.Extensibility;

namespace BookWarehouse.Services.Commands
{
    public class CommandAdd : CommandBase
    {
        private readonly IBookAdder _bookAdder;
        public CommandAdd(IBookAdder bookAdder)
        {
            _bookAdder = bookAdder;
        }
        public override void Execute()
        {
            Book book = new Book();

            Console.WriteLine("Please enter the Name: ");
            string input = Console.ReadLine();
            
            if (String.IsNullOrEmpty(input))
            {
                Console.WriteLine("No Name provided!");
                return;
            }

            book.Name = input;
            Console.WriteLine("Please enter the Author: ");
            input = Console.ReadLine();

            if (String.IsNullOrEmpty(input))
            {
                Console.WriteLine("No Author provided!");
                return;
            }

            book.Author = input;
            Console.WriteLine("Please enter the Number of pages: ");
            input = Console.ReadLine();

            if (!UInt32.TryParse(input, out var numberOfPages))
            {
                Console.WriteLine("No number provided!");
                return;
            }

            book.NumberOfPages = numberOfPages;
            Console.WriteLine("Please enter the genres (one per line, such as {0}) and empty string when you finish:", 
                string.Join(", ", Enum.GetNames(typeof(GenreTypes))));
            input = Console.ReadLine();

            while (!String.IsNullOrEmpty(input))
            {
                if (Enum.TryParse(input, true, out GenreTypes genreType))
                {
                    if (!book.Genres.Contains(genreType))
                    {
                        book.Genres.Add(genreType);
                    }
                    else
                    {
                        Console.WriteLine("This genre is already listed!");
                    }
                }
                else
                {
                    Console.WriteLine("Please enter valid genre!");
                }

                input = Console.ReadLine();
            }

            _bookAdder.AddBook(book);
            Console.WriteLine("Book added successfully!");
        }
    }
}
