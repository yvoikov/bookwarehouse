﻿using System;
using BookWarehouse.DataAccessLayer.Extensibility.Storage;
using BookWarehouse.Services.Extensibility;

namespace BookWarehouse.Services.Commands
{
    public class CommandLoad : CommandBase
    {
        private readonly IBookLoader _bookLoader;

        public CommandLoad(IBookLoader bookLoader)
        {
            _bookLoader = bookLoader;
        }
        public override void Execute()
        {
            Console.WriteLine(_bookLoader.LoadBooks()
                ? "Books loaded successfully!"
                : "An error occured during the loading!"); ;
        }
    }
}
