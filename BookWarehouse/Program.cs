﻿using System.Reflection;
using BookWarehouse.Helpers;
using BookWarehouse.Services.Services;
using Ninject;

namespace BookWarehouse
{
    class Program
    {
        static void Main(string[] args)
        {
            StandardKernel kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            CommandSeeder commandSeeder = new CommandSeeder(kernel);
            CommandService commandService = new CommandService(commandSeeder.Seed());
            commandService.Run();
        }
    }
}
