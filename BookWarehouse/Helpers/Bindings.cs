﻿using System.Collections.Generic;
using BookWareHouse.Data;
using BookWarehouse.DataAccessLayer.Extensibility.Storage;
using BookWarehouse.DataAccessLayer.Storage;
using Ninject.Modules;

namespace BookWarehouse.Helpers
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IBookAdder>().To<BookStorage>();
            Bind<IBookEditor>().To<BookStorage>();
            Bind<IBookGetter>().To<BookStorage>();
            Bind<IBookRemover>().To<BookStorage>();
            Bind<IBookLoader>().To<BookStorage>();
            Bind<IBookSaver>().To<BookStorage>();
            Bind<IList <Book>>().To<List<Book>>().InSingletonScope()
                .WithConstructorArgument("books", new List<Book>());
        }
    }
}
