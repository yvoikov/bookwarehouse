﻿using Ninject;
using BookWarehouse.DataAccessLayer.Extensibility.Storage;
using BookWarehouse.Services.Commands;
using BookWarehouse.Services.Extensibility;

namespace BookWarehouse.Helpers
{
    public class CommandSeeder
    {
        private readonly StandardKernel _kernel;
        public CommandSeeder(StandardKernel kernel)
        {
            _kernel = kernel;
        }
        public CommandBase[] Seed()
        {
            return new CommandBase[]
            {
                new CommandAdd(_kernel.Get<IBookAdder>()),
                new CommandEdit(_kernel.Get<IBookEditor>(), _kernel.Get<IBookGetter>()),
                new CommandShowAllBooks(_kernel.Get<IBookGetter>()), 
                new CommandRemove(_kernel.Get<IBookRemover>(), _kernel.Get<IBookGetter>()), 
                new CommandSave(_kernel.Get<IBookSaver>()), 
                new CommandLoad(_kernel.Get<IBookLoader>()), 
                new CommandQuit(),
            };
        }
    }
}
