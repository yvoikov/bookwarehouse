﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using BookWareHouse.Data;
using BookWarehouse.DataAccessLayer.Extensibility.Storage;

namespace BookWarehouse.DataAccessLayer.Storage
{
    public class BookStorage : IBookAdder, IBookEditor, IBookGetter, IBookRemover, IBookLoader, IBookSaver
    {
        private readonly string SERIALIZED_DATA_PATH = "data.json";

        private IList<Book> _books;
        public BookStorage(IList<Book> books)
        {
            _books = books;
        }
        public void AddBook(Book newBook)
        {
            if (_books.Count == 0)
            {
                newBook.SerialNumber = 1;
            }
            else
            {
                newBook.SerialNumber =  _books.Max(book => book.SerialNumber) + 1;
            }
            
            _books.Add(newBook);
        }

        public void EditBook(Book book)
        {
            var targetBook = _books.FirstOrDefault(b => b.SerialNumber == book.SerialNumber);
            if (targetBook != null)
            {
                int index = _books.IndexOf(targetBook);
                _books.Insert(index, book);
            }
        }

        public List<Book> GetBooks(Func<Book, bool> predicate)
        {
            return _books.Where(predicate).ToList();
        }

        public void RemoveBooks(Book book)
        {
            _books.Remove(book);
        }

        public bool LoadBooks()
        {
            bool result = false;
            if (File.Exists(SERIALIZED_DATA_PATH))
            {
                try
                {
                    string textData = File.ReadAllText(SERIALIZED_DATA_PATH);
                    _books = JsonSerializer.Deserialize<IList<Book>>(textData);
                    result = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return result;
        }

        public bool SaveBooks()
        {
            bool result = false;
            var options = new JsonSerializerOptions
            {
                WriteIndented = true,
            };

            try
            {
                File.WriteAllText(SERIALIZED_DATA_PATH, JsonSerializer.Serialize<IList<Book>>(_books, options));
                result = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return result;
        }
    }
}
