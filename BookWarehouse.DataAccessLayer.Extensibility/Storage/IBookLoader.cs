﻿namespace BookWarehouse.DataAccessLayer.Extensibility.Storage
{
    public interface IBookLoader
    {
        bool LoadBooks();
    }
}
