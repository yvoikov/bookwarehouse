﻿using BookWareHouse.Data;

namespace BookWarehouse.DataAccessLayer.Extensibility.Storage
{
    public interface IBookAdder
    {
        void AddBook(Book newBook);
    }
}
