﻿using System;
using System.Collections.Generic;
using BookWareHouse.Data;

namespace BookWarehouse.DataAccessLayer.Extensibility.Storage
{
    public interface IBookGetter
    {
        List<Book> GetBooks(Func<Book, bool> predicate);
    }
}
