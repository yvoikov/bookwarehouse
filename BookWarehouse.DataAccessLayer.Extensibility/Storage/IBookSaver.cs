﻿namespace BookWarehouse.DataAccessLayer.Extensibility.Storage
{
    public interface IBookSaver
    {
        bool SaveBooks();
    }
}
