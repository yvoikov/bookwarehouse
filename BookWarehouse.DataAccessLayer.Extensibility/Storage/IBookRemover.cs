﻿using BookWareHouse.Data;

namespace BookWarehouse.DataAccessLayer.Extensibility.Storage
{
    public interface IBookRemover
    {
        void RemoveBooks(Book book);
    }
}
