﻿using BookWareHouse.Data;

namespace BookWarehouse.DataAccessLayer.Extensibility.Storage
{
    public interface IBookEditor
    {
        void EditBook(Book book);
    }
}
