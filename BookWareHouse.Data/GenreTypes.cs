﻿namespace BookWareHouse.Data
{
    public enum GenreTypes
    {
        Drama,
        Detective,
        Humor,
        Mystery,
        Romance,
        Adventure,
        FairyTale,
        Fantasy
    }
}
