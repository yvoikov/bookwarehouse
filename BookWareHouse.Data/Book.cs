﻿using System.Collections.Generic;

namespace BookWareHouse.Data
{
    public class Book
    {
        public uint SerialNumber { get; set; }

        public string Name { get; set; }

        public string Author { get; set; }

        public uint NumberOfPages { get; set; }

        public List<GenreTypes> Genres { get; set; }
    }
}
